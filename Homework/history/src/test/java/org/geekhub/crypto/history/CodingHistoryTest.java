package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class CodingHistoryTest {

    private CodingHistory history;

    @BeforeMethod
    public void setUp() {
        history = new CodingHistory();
    }

    private static HistoryRecord codecOperationRecord(
        Operation operation
    ) {
        return HistoryRecord.of(operation, "", Algorithm.MORSE);
    }

    @Test
    public void ensure_that_history_records_is_empty_by_default() {
        assertTrue(history.getHistoryRecords().isEmpty());
    }

    @Test
    public void add_record_was_successful() {
        history.addRecord(codecOperationRecord(Operation.CODEC_ENCODE));

        List<HistoryRecord> historyRecords = history.getHistoryRecords();

        assertEquals(historyRecords, List.of(codecOperationRecord(Operation.CODEC_ENCODE)));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throw_exception_when_record_is_null() {
        history.addRecord(null);
    }

    @Test
    public void clear_not_empty_history_records_was_success() {
        history.addRecord(codecOperationRecord(Operation.CODEC_ENCODE));
        assertFalse(history.getHistoryRecords().isEmpty());

        history.clearHistoryRecords();

        assertTrue(history.getHistoryRecords().isEmpty());
    }

    @Test
    public void clear_empty_history_records_was_success() {
        history.clearHistoryRecords();

        assertTrue(history.getHistoryRecords().isEmpty());
    }

    @Test
    public void remove_last_record_when_history_is_empty_was_success() {
        history.removeLastRecord();

        assertTrue(history.getHistoryRecords().isEmpty());
    }

    @Test
    public void remove_last_record_when_with_one_element_in_history_success() {
        history.addRecord(codecOperationRecord(Operation.CODEC_ENCODE));

        history.removeLastRecord();

        assertTrue(history.getHistoryRecords().isEmpty());
    }

    @Test
    public void remove_last_record_when_with_multiple_elements_in_history_success() {
        history.addRecord(codecOperationRecord(Operation.CODEC_ENCODE));
        history.addRecord(codecOperationRecord(Operation.CODEC_DECODE));

        history.removeLastRecord();

        List<HistoryRecord> historyRecords = history.getHistoryRecords();
        assertEquals(historyRecords, List.of(codecOperationRecord(Operation.CODEC_ENCODE)));
    }
}
