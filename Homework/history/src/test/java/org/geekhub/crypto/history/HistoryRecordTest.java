package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

public class HistoryRecordTest {

    @Test
    public void create_instance_by_using_factory_method() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        assertThat(historyRecord).isNotNull();
        assertThat(historyRecord.getId()).isNull();
        assertThat(historyRecord.getDate()).isEqualTo(LocalDate.now());
        assertThat(historyRecord.getOperation()).isEqualTo(Operation.CODEC_DECODE);
        assertThat(historyRecord.getUserInput()).isEmpty();
        assertThat(historyRecord.getAlgorithm()).isEqualTo(Algorithm.MORSE);
    }

    @Test
    public void create_instance_with_id() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        var historyRecordWithId = historyRecord.withId(1);

        assertThat(historyRecordWithId).isNotNull();
        assertThat(historyRecordWithId.getId()).isEqualTo(1);
        assertThat(historyRecordWithId.getDate()).isEqualTo(LocalDate.now());
        assertThat(historyRecordWithId.getOperation()).isEqualTo(Operation.CODEC_DECODE);
        assertThat(historyRecordWithId.getUserInput()).isEmpty();
        assertThat(historyRecordWithId.getAlgorithm()).isEqualTo(Algorithm.MORSE);
    }

    @Test
    public void verify_equals_with_null() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        assertThat(historyRecord).isNotEqualTo(null);
    }

    @Test
    public void verify_equals_with_same() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        assertThat(historyRecord).isEqualTo(historyRecord);
        assertThat(historyRecord).isSameAs(historyRecord);
    }

    @Test
    public void verify_equals() {
        var historyRecord1 = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );
        var historyRecord2 = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        assertThat(historyRecord1).isEqualTo(historyRecord2);
    }

    @Test
    public void verify_hashcode() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        );

        var historyRecordWithId = historyRecord.withId(1);
        var historyRecordForHashCodeCheck = historyRecord.withId(1);

        assertThat(historyRecordWithId).hasSameHashCodeAs(historyRecordForHashCodeCheck);
    }

    @Test
    public void verify_to_string_is_implemented() {
        final var year = 2019;
        final var dayOfMonth = 14;

        var historyRecord = HistoryRecord.of(
            LocalDate.of(year, Month.JANUARY, dayOfMonth),
            Operation.CODEC_DECODE,
            "",
            Algorithm.MORSE
        ).withId(1);

        assertThat(historyRecord).hasToString(
            "HistoryRecord{id=1, date=2019-01-14, operation=CODEC_DECODE, userInput='', algorithm=MORSE}"
        );
    }
}
