CREATE TABLE history_record
(
    id         SERIAL       NOT NULL PRIMARY KEY,
    date       DATE         NOT NULL,
    operation  VARCHAR(128) NOT NULL,
    user_input TEXT         NOT NULL,
    algorithm  VARCHAR(128) NOT NULL
);
