package org.geekhub.crypto.history;

import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class CodingHistory {

    private final LinkedList<HistoryRecord> historyRecords;

    public CodingHistory() {
        this.historyRecords = new LinkedList<>();
    }

    public List<HistoryRecord> getHistoryRecords() {
        return historyRecords;
    }

    public void addRecord(HistoryRecord historyRecord) {
        if (historyRecord == null) {
            throw new IllegalArgumentException("historyRecord must be not null");
        }

        historyRecords.add(historyRecord);
    }

    public void removeLastRecord() {
        historyRecords.pollLast();
    }

    public void clearHistoryRecords() {
        historyRecords.clear();
    }
}
