package org.geekhub.crypto.history;

public enum Operation {
    CODEC_ENCODE,
    CODEC_DECODE
}
