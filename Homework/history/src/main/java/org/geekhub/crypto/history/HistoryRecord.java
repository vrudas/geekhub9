package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class HistoryRecord {

    @Id
    private final Integer id;
    private final LocalDate date;
    private final Operation operation;
    private final String userInput;
    private final Algorithm algorithm;

    public static HistoryRecord of(
        Operation operation,
        String userInput,
        Algorithm algorithm
    ) {
        return of(
            LocalDate.now(),
            operation,
            userInput,
            algorithm
        );
    }

    public static HistoryRecord of(
        LocalDate date,
        Operation operation,
        String userInput,
        Algorithm algorithm
    ) {
        return new HistoryRecord(
            null,
            operation,
            date,
            userInput,
            algorithm
        );
    }

    HistoryRecord(
        Integer id,
        Operation operation,
        LocalDate date,
        String userInput,
        Algorithm algorithm
    ) {
        this.id = id;
        this.algorithm = requireNonNull(algorithm);
        this.date = date;
        this.operation = requireNonNull(operation);
        this.userInput = userInput;
    }

    public HistoryRecord withId(Integer id) {
        return new HistoryRecord(
            id,
            this.operation,
            this.date,
            this.userInput,
            this.algorithm
        );
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Operation getOperation() {
        return operation;
    }

    public String getUserInput() {
        return userInput;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HistoryRecord that = (HistoryRecord) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            operation == that.operation &&
            Objects.equals(userInput, that.userInput) &&
            algorithm == that.algorithm;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, operation, userInput, algorithm);
    }

    @Override
    public String toString() {
        return "HistoryRecord{" +
            "id=" + id +
            ", date=" + date +
            ", operation=" + operation +
            ", userInput='" + userInput + '\'' +
            ", algorithm=" + algorithm +
            '}';
    }
}
