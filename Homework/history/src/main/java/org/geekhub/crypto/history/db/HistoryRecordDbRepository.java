package org.geekhub.crypto.history.db;

import org.geekhub.crypto.history.HistoryRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryRecordDbRepository extends CrudRepository<HistoryRecord, Integer> {
}
