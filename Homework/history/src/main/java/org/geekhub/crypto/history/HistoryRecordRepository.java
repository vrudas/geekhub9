package org.geekhub.crypto.history;

import java.util.List;

public interface HistoryRecordRepository {

    List<HistoryRecord> getHistoryRecords();

    void addRecord(HistoryRecord historyRecord);

    void removeLastRecord();

    void clearHistoryRecords();
}
