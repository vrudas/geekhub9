package org.geekhub.crypto.coders;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MorseCodecTest {

    private MorseCodec morseCodec;

    @BeforeMethod
    public void setUp() {
        morseCodec = new MorseCodec();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void encode_with_null_input() {
        morseCodec.encode(null);
    }

    @Test
    public void encode_with_empty_input() {
        assertEquals(morseCodec.encode(""), "");
    }

    @Test
    public void encode_space_character() {
        assertEquals(morseCodec.encode(" "), ".......");
    }

    @Test
    public void encode_unsupported_character() {
        assertEquals(morseCodec.encode("~"), "........");
    }

    @Test
    public void encode_numbers() {
        assertEquals(
            morseCodec.encode("1234567890"),
            ".----/..---/...--/....-/...../-..../--.../---../----./-----"
        );
    }

    @Test
    public void encode_word_in_lower_case() {
        assertEquals(
            morseCodec.encode("morse"),
            "--/---/.-./.../."
        );
    }

    @Test
    public void encode_word_in_upper_case() {
        assertEquals(
            morseCodec.encode("MORSE"),
            "--/---/.-./.../."
        );
    }

    @Test
    public void encode_multiple_words() {
        assertEquals(
            morseCodec.encode("HELLO world"),
            "...././.-../.-../---/......./.--/---/.-./.-../-.."
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_with_null_input() {
        morseCodec.decode(null);
    }

    @Test
    public void decode_with_empty_input() {
        morseCodec.decode("");
    }

    @Test
    public void decode_space_character() {
        assertEquals(morseCodec.decode("......."), " ");
    }

    @Test
    public void decode_unsupported_character() {
        assertEquals(morseCodec.decode("........"), " ");
    }

    @Test
    public void decode_numbers() {
        assertEquals(
            morseCodec.decode(".----/..---/...--/....-/...../-..../--.../---../----./-----"),
            "1234567890"
        );
    }

    @Test
    public void decode_word_in_upper_case() {
        assertEquals(
            morseCodec.decode("--/---/.-./.../."),
            "MORSE"
        );
    }

    @Test
    public void decode_multiple_words() {
        assertEquals(
            morseCodec.decode("...././.-../.-../---/......./.--/---/.-./.-../-.."),
            "HELLO WORLD"
        );
    }

    @Test
    public void check_codec_type() {
        assertEquals(morseCodec.type(), Algorithm.MORSE);
    }
}
