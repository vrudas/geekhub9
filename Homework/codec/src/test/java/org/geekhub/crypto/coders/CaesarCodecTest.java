package org.geekhub.crypto.coders;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CaesarCodecTest {

    private CaesarCodec caesarCodec;

    @BeforeMethod
    public void setUp() {
        final int seed = 42;
        caesarCodec = new CaesarCodec(seed);
    }

    @Test
    public void creation_with_default_constructor_was_success() {
        caesarCodec = new CaesarCodec();
        assertNotNull(caesarCodec);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void encode_with_null_input() {
        caesarCodec.encode(null);
    }

    @Test
    public void encode_with_empty_input() {
        caesarCodec.encode("");
    }

    @Test
    public void encode_space_character() {
        assertEquals(caesarCodec.encode(" "), "F");
    }

    @Test
    public void encode_unsupported_character() {
        assertEquals(caesarCodec.encode("~"), " ");
    }

    @Test
    public void encode_numbers() {
        assertEquals(
            caesarCodec.encode("1234567890"),
            "HIJKLMNOPG"
        );
    }

    @Test
    public void encode_word_in_lower_case() {
        assertEquals(
            caesarCodec.encode("caesar"),
            "SQU7Q6"
        );
    }

    @Test
    public void encode_word_in_upper_case() {
        assertEquals(
            caesarCodec.encode("CAESAR"),
            "hfjxfw"
        );
    }

    @Test
    public void encode_multiple_words() {
        assertEquals(
            caesarCodec.encode("HELLO world"),
            "mjqqtFb360T"
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_with_null_input() {
        caesarCodec.decode(null);
    }

    @Test
    public void decode_with_empty_input() {
        caesarCodec.decode("");
    }

    @Test
    public void decode_space_character() {
        assertEquals(caesarCodec.decode("F"), " ");
    }

    @Test
    public void decode_unsupported_character() {
        assertEquals(caesarCodec.decode("~"), " ");
    }

    @Test
    public void decode_numbers() {
        assertEquals(
            caesarCodec.decode("HIJKLMNOPG"),
            "1234567890"
        );
    }

    @Test
    public void decode_word_in_upper_case() {
        assertEquals(
            caesarCodec.decode("hfjxfw"),
            "CAESAR"
        );
    }

    @Test
    public void decode_word_in_lower_case() {
        assertEquals(
            caesarCodec.decode("SQU7Q6"),
            "caesar"
        );
    }

    @Test
    public void decode_multiple_words() {
        assertEquals(
            caesarCodec.decode("mjqqtFb360T"),
            "HELLO world"
        );
    }

    @Test
    public void check_codec_type() {
        assertEquals(caesarCodec.type(), Algorithm.CAESAR);
    }
}
