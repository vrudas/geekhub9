package org.geekhub.crypto.coders;

import org.springframework.stereotype.Component;

@Component
class CaesarCodec implements Codec {

    private static final char DEFAULT_CHARACTER = ' ';

    private static final char[] SUPPORTED_CHARS = {
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    private static final int DEFAULT_SEED = 42;

    private final int seed;

    CaesarCodec() {
        this(DEFAULT_SEED);
    }

    CaesarCodec(int seed) {
        this.seed = seed;
    }

    @Override
    public String encode(String input) {
        validateInput(input);

        return shiftLetters(input, seed);
    }

    @Override
    public String decode(String input) {
        validateInput(input);

        return shiftLetters(input, -seed);
    }

    private void validateInput(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Illegal input: 'null'");
        }
    }

    private String shiftLetters(String input, int shift) {
        StringBuilder sb = new StringBuilder();

        for (char c : input.toCharArray()) {
            sb.append(shiftChar(c, shift));
        }

        return sb.toString();
    }

    private char shiftChar(char c, int shift) {
        final int index = indexInSupportedChars(c);
        if (index >= 0) {
            final int shiftedIndex = (index + shift + SUPPORTED_CHARS.length) % SUPPORTED_CHARS.length;
            return SUPPORTED_CHARS[shiftedIndex];
        } else {
            return DEFAULT_CHARACTER;
        }
    }

    private int indexInSupportedChars(char c) {
        for (int i = 0; i < SUPPORTED_CHARS.length; i++) {
            if (SUPPORTED_CHARS[i] == c) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public Algorithm type() {
        return Algorithm.CAESAR;
    }
}
