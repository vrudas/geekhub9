package org.geekhub.crypto.coders;

public interface Codec extends Encoder, Decoder {

    Algorithm type();

}
