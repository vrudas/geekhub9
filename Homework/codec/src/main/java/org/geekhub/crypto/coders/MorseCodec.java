package org.geekhub.crypto.coders;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static java.util.Map.entry;

@Component
class MorseCodec implements Codec {

    private static final String SYMBOL_SEPARATOR = "/";

    private static final String ERROR_SYMBOL = "........";

    private static final Map<Character, String> LATIN_TO_MORSE = Map.ofEntries(
        entry('A', ".-"),
        entry('B', "-..."),
        entry('C', "-.-."),
        entry('D', "-.."),
        entry('E', "."),
        entry('F', "..-."),
        entry('G', "--."),
        entry('H', "...."),
        entry('I', ".."),
        entry('J', ".---"),
        entry('K', "-.-"),
        entry('L', ".-.."),
        entry('M', "--"),
        entry('N', "-."),
        entry('O', "---"),
        entry('P', ".--."),
        entry('Q', "--.-"),
        entry('R', ".-."),
        entry('S', "..."),
        entry('T', "-"),
        entry('U', "..-"),
        entry('V', "...-"),
        entry('W', ".--"),
        entry('X', "-..-"),
        entry('Y', "-.--"),
        entry('Z', "--.."),
        entry(' ', "......."),
        entry('1', ".----"),
        entry('2', "..---"),
        entry('3', "...--"),
        entry('4', "....-"),
        entry('5', "....."),
        entry('6', "-...."),
        entry('7', "--..."),
        entry('8', "---.."),
        entry('9', "----."),
        entry('0', "-----")
    );

    private static final Map<String, Character> MORSE_TO_LATIN = initMorseToLatin();

    private static Map<String, Character> initMorseToLatin() {
        Map<String, Character> inverseMap = new HashMap<>();

        for (var latinToMorse : MorseCodec.LATIN_TO_MORSE.entrySet()) {
            var latin = latinToMorse.getKey();
            var morse = latinToMorse.getValue();

            inverseMap.put(morse, latin);
        }

        return inverseMap;
    }

    @Override
    public String encode(String input) {
        validateInput(input);

        StringBuilder sb = new StringBuilder();

        for (char c : input.toUpperCase().toCharArray()) {
            sb.append(LATIN_TO_MORSE.getOrDefault(c, ERROR_SYMBOL))
                .append(SYMBOL_SEPARATOR);
        }

        if (sb.length() > 0) {
            return sb.substring(0, sb.length() - 1);
        }

        return sb.toString();
    }

    @Override
    public String decode(String input) {
        validateInput(input);

        StringBuilder sb = new StringBuilder();

        for (String morseCode : input.split(SYMBOL_SEPARATOR)) {
            sb.append(MORSE_TO_LATIN.getOrDefault(morseCode, ' '));
        }

        return sb.toString();
    }

    private void validateInput(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Illegal input: 'null'");
        }
    }

    @Override
    public Algorithm type() {
        return Algorithm.MORSE;
    }
}
