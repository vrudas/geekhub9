package org.geekhub.crypto.analytics;

import org.geekhub.crypto.coders.Algorithm;
import org.geekhub.crypto.history.CodingHistory;
import org.geekhub.crypto.history.HistoryRecord;
import org.geekhub.crypto.history.Operation;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@Component
public class CodingAudit {

    private static final Map<Operation, CodecUsecase> OPERATION_TO_CODEC_USE_CASE = Map.ofEntries(
        entry(Operation.CODEC_ENCODE, CodecUsecase.ENCODING),
        entry(Operation.CODEC_DECODE, CodecUsecase.DECODING)
    );

    private final CodingHistory codingHistory;

    public CodingAudit(CodingHistory codingHistory) {
        this.codingHistory = codingHistory;
    }

    /**
     * Find words repetitions across all encoding inputs in descending order.
     *
     * @return Map<Word, OccurrenceCount>
     */
    Map<String, Long> countEncodingInputs() {
        return codingHistory.getHistoryRecords().stream()
            .map(HistoryRecord::getUserInput)
            .filter(Objects::nonNull)
            .map(input -> input.split(" "))
            .flatMap(Stream::of)
            .collect(groupingBy(Function.identity(), counting()));
    }

    /**
     * Count operations done per day.
     *
     * @param usecase - type of coding operation: encoding/decoding
     * @return -  Map<Date, OperationsCount>
     */
    Map<LocalDate, Long> countCodingsByDate(CodecUsecase usecase) {
        List<HistoryRecord> records = filterRecordsByUsecase(usecase);
        List<LocalDate> dates = extractDatesFromHistory(records);

        return countOccurrences(dates);
    }

    private List<LocalDate> extractDatesFromHistory(List<HistoryRecord> records) {
        List<LocalDate> dates = new ArrayList<>();
        for (var record : records) {
            if (record.getDate() != null) {
                dates.add(record.getDate());
            }
        }
        return dates;
    }

    /**
     * Find the algorithm used the most times for usecase.
     *
     * @param usecase - type of coding operation: encoding/decoding
     * @return - top used algorithm
     */
    Optional<Algorithm> findMostPopularCodec(CodecUsecase usecase) {
        List<HistoryRecord> records = filterRecordsByUsecase(usecase);
        List<Algorithm> algorithms = extractUsedAlgorithms(records);
        Map<Algorithm, Long> algorithmToCount = countOccurrences(algorithms);

        return Optional.ofNullable(extractMostPopularCodec(algorithmToCount));
    }

    private List<HistoryRecord> filterRecordsByUsecase(CodecUsecase usecase) {
        return codingHistory.getHistoryRecords().stream()
            .filter(record -> OPERATION_TO_CODEC_USE_CASE.containsKey(record.getOperation()))
            .filter(record -> usecase == OPERATION_TO_CODEC_USE_CASE.get(record.getOperation()))
            .collect(Collectors.toList());
    }

    private List<Algorithm> extractUsedAlgorithms(List<HistoryRecord> records) {
        List<Algorithm> algorithms = new ArrayList<>();

        for (var record : records) {
            Algorithm algorithm = record.getAlgorithm();
            if (algorithm != null) {
                algorithms.add(algorithm);
            }
        }

        return algorithms;
    }

    private Algorithm extractMostPopularCodec(Map<Algorithm, Long> algorithmToCount) {
        if (algorithmToCount.isEmpty()) {
            return null;
        }

        return Collections.max(algorithmToCount.entrySet(), Map.Entry.comparingByValue()).getKey();
    }

    private <K> Map<K, Long> countOccurrences(List<K> elements) {
        var occurrenceCount = new HashMap<K, Long>();

        for (var element : elements) {
            Long elementCount = occurrenceCount.getOrDefault(element, 0L);
            occurrenceCount.put(element, elementCount + 1L);
        }

        return occurrenceCount;
    }
}
