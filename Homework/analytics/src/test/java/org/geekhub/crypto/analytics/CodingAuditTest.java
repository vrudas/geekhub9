package org.geekhub.crypto.analytics;

import org.geekhub.crypto.coders.Algorithm;
import org.geekhub.crypto.history.CodingHistory;
import org.geekhub.crypto.history.HistoryRecord;
import org.geekhub.crypto.history.Operation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.*;
import static java.util.Map.entry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CodingAuditTest {

    private static HistoryRecord createEncodingHistoryRecord(String userInput) {
        return HistoryRecord.of(
            Operation.CODEC_ENCODE,
            userInput,
            Algorithm.CAESAR
        );
    }

    private static HistoryRecord createCodecHistoryRecord(Algorithm algorithm) {
        return HistoryRecord.of(
            Operation.CODEC_ENCODE,
            "encode",
            algorithm
        );
    }

    private void assertMostPopularCodec(
        Algorithm expectedMostPopularCodec,
        List<HistoryRecord> historyRecords
    ) {
        when(codingHistory.getHistoryRecords()).thenReturn(historyRecords);

        Optional<Algorithm> mostPopularCodec = codingAudit.findMostPopularCodec(CodecUsecase.ENCODING);

        assertEquals(mostPopularCodec, Optional.ofNullable(expectedMostPopularCodec));
    }

    private void assertEncodingInputCounts(
        List<HistoryRecord> historyRecords,
        Map<String, Long> expectedEncodingInputCounts
    ) {
        when(codingHistory.getHistoryRecords()).thenReturn(historyRecords);

        var encodingInputCounts = codingAudit.countEncodingInputs();

        assertEquals(encodingInputCounts, expectedEncodingInputCounts);
    }

    private void assertCodingCountByDate(
        CodecUsecase codecUsecase,
        List<HistoryRecord> historyRecords,
        Map<LocalDate, Long> expectedCodingsByDate
    ) {
        when(codingHistory.getHistoryRecords()).thenReturn(historyRecords);

        var codingsCountByDate = codingAudit.countCodingsByDate(codecUsecase);

        assertEquals(codingsCountByDate, expectedCodingsByDate);
    }

    private CodingHistory codingHistory;

    private CodingAudit codingAudit;

    @BeforeMethod
    public void setUp() {
        codingHistory = mock(CodingHistory.class);

        codingAudit = new CodingAudit(codingHistory);
    }

    @Test
    public void count_encoding_inputs_for_empty_history() {
        List<HistoryRecord> historyRecords = emptyList();
        Map<String, Long> expectedEncodingInputCounts = emptyMap();

        assertEncodingInputCounts(historyRecords, expectedEncodingInputCounts);
    }

    @Test
    public void count_encoding_inputs_for_history_with_one_element() {
        assertEncodingInputCounts(
            List.of(createEncodingHistoryRecord("first")),
            Map.of("first", 1L)
        );
    }

    @Test
    public void count_encoding_inputs() {
        var historyRecords = List.of(
            createEncodingHistoryRecord("first"),
            createEncodingHistoryRecord("first second"),
            createEncodingHistoryRecord("first second third")
        );

        final var firstInputCount = 3L;
        final var secondInputCount = 2L;
        final var thirdInputCount = 1L;

        Map<String, Long> expectedEncodingInputCounts = Map.ofEntries(
            entry("first", firstInputCount),
            entry("second", secondInputCount),
            entry("third", thirdInputCount)
        );

        assertEncodingInputCounts(historyRecords, expectedEncodingInputCounts);
    }

    @Test
    public void find_most_popular_codec_returns_null_for_empty_coding_history() {
        assertMostPopularCodec(null, emptyList());
    }

    @Test
    public void find_most_popular_codec_with_coding_history_single_element() {
        var expectedMostPopularCodec = Algorithm.CAESAR;
        var historyRecords = List.of(createCodecHistoryRecord(expectedMostPopularCodec));

        assertMostPopularCodec(expectedMostPopularCodec, historyRecords);
    }

    @Test
    public void find_most_popular_codec_with_coding_history_multiple_elements() {
        var expectedMostPopularCodec = Algorithm.CAESAR;
        var historyRecords = List.of(
            createCodecHistoryRecord(Algorithm.MORSE),
            createCodecHistoryRecord(expectedMostPopularCodec),
            createCodecHistoryRecord(expectedMostPopularCodec)
        );

        assertMostPopularCodec(expectedMostPopularCodec, historyRecords);
    }

    @Test
    public void count_encodings_by_date_for_empty_history() {
        assertCodingCountByDate(
            CodecUsecase.ENCODING,
            emptyList(),
            emptyMap()
        );
    }

    @Test
    public void count_encodings_by_date_for_history_without_decoding_use_case() {
        var historyRecords = List.of(
            createCodecHistoryRecord(Algorithm.MORSE)
        );

        assertCodingCountByDate(
            CodecUsecase.DECODING,
            historyRecords,
            emptyMap()
        );
    }

    @Test
    public void count_encodings_by_date() {
        var historyRecords = List.of(
            createCodecHistoryRecord(Algorithm.MORSE),
            createCodecHistoryRecord(Algorithm.CAESAR)
        );

        var expectedCodingsByDate = singletonMap(
            LocalDate.now(),
            (long) historyRecords.size()
        );

        assertCodingCountByDate(
            CodecUsecase.ENCODING,
            historyRecords,
            expectedCodingsByDate
        );
    }

}
