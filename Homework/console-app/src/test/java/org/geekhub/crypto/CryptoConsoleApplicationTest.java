package org.geekhub.crypto;

import org.geekhub.crypto.coders.Algorithm;
import org.geekhub.crypto.coders.Codec;
import org.geekhub.crypto.input.InputOperation;
import org.geekhub.crypto.input.InputOperationReader;
import org.geekhub.crypto.ui.MenuPrinter;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.*;

public class CryptoConsoleApplicationTest {

    private CryptoConsoleApplication initApplication(Codec... codecs) {
        return new CryptoConsoleApplication(
            List.of(codecs),
            menuPrinter,
            inputOperationReader
        );
    }

    @Mock
    private MenuPrinter menuPrinter;

    @Mock
    private InputOperationReader inputOperationReader;

    @Mock
    private Codec codec;

    private CryptoConsoleApplication application;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        application = initApplication(codec);
    }

    @Test
    public void application_closed_because_of_no_codecs_for_test_coding() {
        application = initApplication();

        application.run();

        verify(menuPrinter).printNoCodecsMessage();
        verifyNoMoreInteractions(inputOperationReader);
    }

    @Test
    public void application_closed_because_of_exit_operation_was_chosen() {
        when(inputOperationReader.readInputOperation())
            .thenReturn(InputOperation.EXIT);

        application.run();

        verify(menuPrinter).printMenu();
        verify(menuPrinter).printChoseOperationMessage();
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void test_coding_failed_because_of_not_correctly_decoded_text() {
        when(codec.type()).thenReturn(Algorithm.CAESAR);
        when(codec.encode(anyString())).thenReturn("");
        when(codec.decode(anyString())).thenReturn("");

        application = initApplication(codec);

        when(inputOperationReader.readInputOperation())
            .thenReturn(InputOperation.TEST_CODING);

        application.run();

        verify(menuPrinter).printMenu();
        verify(menuPrinter).printChoseOperationMessage();

        verify(menuPrinter).printEncodedMessage(anyString());
        verify(menuPrinter).printDecodedMessage(anyString());

        verify(menuPrinter, never()).printTestCodingCompletedMessage();
    }

    @Test
    public void test_coding_completed() {
        when(codec.type()).thenReturn(Algorithm.CAESAR);
        when(codec.encode(anyString())).thenReturn("caesar CAESAR");
        when(codec.decode(anyString())).thenReturn("CAESAR caesar");

        application = initApplication(codec);

        when(inputOperationReader.readInputOperation())
            .thenReturn(InputOperation.TEST_CODING)
            .thenReturn(InputOperation.EXIT);

        application.run();

        verify(menuPrinter, times(2)).printMenu();
        verify(menuPrinter, times(2)).printChoseOperationMessage();

        verify(menuPrinter).printEncodedMessage(anyString());
        verify(menuPrinter).printDecodedMessage(anyString());

        verify(menuPrinter).printTestCodingCompletedMessage();
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void test_coding_failed_because_no_required_codec_found() {
        when(codec.type()).thenReturn(Algorithm.MORSE);

        application = initApplication(codec);

        when(inputOperationReader.readInputOperation())
            .thenReturn(InputOperation.TEST_CODING);

        application.run();
    }

}