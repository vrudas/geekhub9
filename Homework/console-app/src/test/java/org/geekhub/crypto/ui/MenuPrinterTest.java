package org.geekhub.crypto.ui;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class MenuPrinterTest {

    private static void configureSystemOutForTest(ByteArrayOutputStream outputStream) {
        System.setOut(new PrintStream(outputStream));
    }
    private String retrieveConsoleOutput() {
        return outputStream.toString();
    }

    private PrintStream systemPrintStream;

    private ByteArrayOutputStream outputStream;

    private MenuPrinter menuPrinter;

    @BeforeMethod
    public void setUp() {
        systemPrintStream = System.out;

        outputStream = new ByteArrayOutputStream();

        configureSystemOutForTest(outputStream);

        menuPrinter = new MenuPrinter();
    }

    @AfterMethod
    public void tearDown() throws IOException {
        System.setOut(systemPrintStream);
        outputStream.close();
    }

    @Test
    public void verify_menu_printed() {
        menuPrinter.printMenu();

        var actualMenuOutput = retrieveConsoleOutput();

        assertThat(actualMenuOutput).isEqualTo(
            System.lineSeparator() +
                "+================+" + System.lineSeparator() +
                "| 1. Test Coding |" + System.lineSeparator() +
                "| 0. Exit        |" + System.lineSeparator() +
                "+================+" + System.lineSeparator() +
                System.lineSeparator()
        );
    }

    @Test
    public void verify_chose_operation_message_printed() {
        menuPrinter.printChoseOperationMessage();

        var choseOperationMessage = retrieveConsoleOutput();

        assertThat(choseOperationMessage).isEqualTo("Chose operation:" + System.lineSeparator());
    }

    @Test
    public void verify_encoded_message_printed() {
        menuPrinter.printEncodedMessage("encoded");

        var encodedMessage = retrieveConsoleOutput();

        assertThat(encodedMessage).isEqualTo("Encoded: encoded" + System.lineSeparator());
    }

    @Test
    public void verify_decoded_message_printed() {
        menuPrinter.printDecodedMessage("decoded");

        var decodedMessage = retrieveConsoleOutput();

        assertThat(decodedMessage).isEqualTo("Decoded: decoded" + System.lineSeparator());
    }

    @Test
    public void verify_coding_test_completed_message_printed() {
        menuPrinter.printTestCodingCompletedMessage();

        var codingCompletedMessage = retrieveConsoleOutput();

        assertThat(codingCompletedMessage)
            .isEqualTo("Test coding completed without errors" + System.lineSeparator());
    }

    @Test
    public void verify_no_codecs_message_printed() {
        menuPrinter.printNoCodecsMessage();

        var noCodecsMessage = retrieveConsoleOutput();

        assertThat(noCodecsMessage)
            .isEqualTo("No codecs to perform coding testing" + System.lineSeparator());
    }
}