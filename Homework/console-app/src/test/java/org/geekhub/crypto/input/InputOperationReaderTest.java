package org.geekhub.crypto.input;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

public class InputOperationReaderTest {

    private static InputOperationReader initInputOperationReader(String source) {
        var scanner = new Scanner(source);
        return new InputOperationReader(scanner);
    }

    private InputOperationReader inputOperationReader;

    @AfterMethod
    public void tearDown() {
        inputOperationReader.shutdown();
    }

    @Test(expectedExceptions = NoSuchElementException.class)
    public void read_input_operation_failed_because_of_empty_input() {
        inputOperationReader = initInputOperationReader("");

        inputOperationReader.readInputOperation();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void read_input_operation_failed_because_of_illegal_input() {
        inputOperationReader = initInputOperationReader("-1");

        inputOperationReader.readInputOperation();
    }

    @Test
    public void read_exit_input_operation() {
        inputOperationReader = initInputOperationReader("0");

        InputOperation inputOperation = inputOperationReader.readInputOperation();

        assertThat(inputOperation).isEqualTo(InputOperation.EXIT);
    }

    @Test
    public void read_test_coding_input_operation() {
        inputOperationReader = initInputOperationReader("1");

        InputOperation inputOperation = inputOperationReader.readInputOperation();

        assertThat(inputOperation).isEqualTo(InputOperation.TEST_CODING);
    }

    @Test
    public void shutdown_completed_successfully() {
        inputOperationReader = new InputOperationReader();

        assertThatCode(() -> inputOperationReader.shutdown())
            .doesNotThrowAnyException();
    }
}