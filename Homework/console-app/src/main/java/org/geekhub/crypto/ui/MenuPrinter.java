package org.geekhub.crypto.ui;

import org.springframework.stereotype.Component;


@Component
@SuppressWarnings("squid:S106")
public class MenuPrinter {

    public void printMenu() {
        System.out.println();
        System.out.println("+================+");
        System.out.println("| 1. Test Coding |");
        System.out.println("| 0. Exit        |");
        System.out.println("+================+");
        System.out.println();
    }

    public void printChoseOperationMessage() {
        System.out.println("Chose operation:");
    }

    public void printEncodedMessage(String encodedText) {
        System.out.println("Encoded: " + encodedText);
    }

    public void printDecodedMessage(String decodedText) {
        System.out.println("Decoded: " + decodedText);
    }

    public void printTestCodingCompletedMessage() {
        System.out.println("Test coding completed without errors");
    }

    public void printNoCodecsMessage() {
        System.out.println("No codecs to perform coding testing");
    }
}
