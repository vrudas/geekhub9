package org.geekhub.crypto;

import org.geekhub.crypto.coders.Algorithm;
import org.geekhub.crypto.coders.Codec;
import org.geekhub.crypto.input.InputOperation;
import org.geekhub.crypto.input.InputOperationReader;
import org.geekhub.crypto.ui.MenuPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@SuppressWarnings("squid:S106")
@SpringBootApplication
public class CryptoConsoleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CryptoConsoleApplication.class, args);
    }

    private final Map<Algorithm, Codec> codecs;
    private final MenuPrinter menuPrinter;
    private final InputOperationReader inputOperationReader;

    @Autowired
    public CryptoConsoleApplication(
        List<Codec> codecs,
        MenuPrinter menuPrinter,
        InputOperationReader inputOperationReader
    ) {
        this.codecs = initCodecTypeToCodec(codecs);
        this.menuPrinter = menuPrinter;
        this.inputOperationReader = inputOperationReader;
    }

    private Map<Algorithm, Codec> initCodecTypeToCodec(List<Codec> codecs) {
        return codecs.stream().collect(toMap(Codec::type, Function.identity()));
    }

    @Override
    public void run(String... args) {
        if (codecs.isEmpty()) {
            menuPrinter.printNoCodecsMessage();
            return;
        }

        performOperation();
    }

    private void performOperation() {
        menuPrinter.printMenu();

        menuPrinter.printChoseOperationMessage();
        InputOperation inputOperation = inputOperationReader.readInputOperation();

        switch (inputOperation) {
            case TEST_CODING:
                testCoding();
                break;
            case EXIT:
            default:
                break;
        }
    }

    private void testCoding() {
        Algorithm algorithm = Algorithm.CAESAR;
        String initialString = algorithm.name() + " " + algorithm.name().toLowerCase();

        Codec codec = getCodec(algorithm);

        String encoded = codec.encode(initialString);
        menuPrinter.printEncodedMessage(encoded);

        String decoded = codec.decode(encoded);
        menuPrinter.printDecodedMessage(decoded);

        if (!Objects.equals(initialString, decoded)) {
            throw new IllegalStateException("Initial and decoded strings mismatch");
        }

        menuPrinter.printTestCodingCompletedMessage();

        performOperation();
    }

    private Codec getCodec(Algorithm algorithm) {
        if (codecs.containsKey(algorithm)) {
            return codecs.get(algorithm);
        }
        throw new IllegalStateException("Codec '" + algorithm + "' not found");
    }

}
