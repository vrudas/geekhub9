package org.geekhub.crypto.input;

import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.Scanner;

@Component
public class InputOperationReader {

    private final Scanner scanner;

    public InputOperationReader() {
        this(new Scanner(System.in));
    }

    InputOperationReader(Scanner scanner) {
        this.scanner = scanner;
    }

    public InputOperation readInputOperation() {
        int operationId = scanner.nextInt();

        switch (operationId) {
            case 0:
                return InputOperation.EXIT;
            case 1:
                return InputOperation.TEST_CODING;
            default:
                throw new IllegalArgumentException("Input operation not supported for id: " + operationId);
        }
    }

    @PreDestroy
    public void shutdown() {
        scanner.close();
    }
}
