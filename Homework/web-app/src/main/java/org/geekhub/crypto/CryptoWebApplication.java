package org.geekhub.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("CheckStyle")
public class CryptoWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CryptoWebApplication.class, args);
    }
}
