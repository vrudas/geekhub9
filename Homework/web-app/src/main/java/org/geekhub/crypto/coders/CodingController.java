package org.geekhub.crypto.coders;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@RestController
@RequestMapping("/coding")
public class CodingController {

    private final Map<Algorithm, Codec> codecs;

    public CodingController(
        List<Codec> codecs
    ) {
        this.codecs = initCodecTypeToCodec(codecs);
    }

    private Map<Algorithm, Codec> initCodecTypeToCodec(List<Codec> codecs) {
        return codecs.stream().collect(toMap(Codec::type, Function.identity()));
    }

    @PostMapping("/encode/{codec}")
    public String encode(
        @PathVariable Algorithm codec,
        @RequestParam String input
    ) {
        return codecs.get(codec).encode(input);
    }

    @PostMapping("/decode/{codec}")
    public String decode(
        @PathVariable Algorithm codec,
        @RequestParam String input
    ) {
        return codecs.get(codec).decode(input);
    }

}
