package org.geekhub.crypto.analytics;

import org.geekhub.crypto.coders.Algorithm;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

@RestController
@RequestMapping("/analytics")
public class AnalyticsController {

    private final CodingAudit codingAudit;

    public AnalyticsController(CodingAudit codingAudit) {
        this.codingAudit = codingAudit;
    }

    @GetMapping("/encoding-inputs-count")
    public Map<String, Long> countEncodingInputs() {
        return codingAudit.countEncodingInputs();
    }

    @GetMapping("/codings-by-date/{usecase}")
    public Map<LocalDate, Long> countCodingsByDate(@PathVariable CodecUsecase usecase) {
        return codingAudit.countCodingsByDate(usecase);
    }

    @GetMapping("/most-popular-codec/{usecase}")
    public Algorithm findMostPopularCodec(@PathVariable CodecUsecase usecase) {
        return codingAudit.findMostPopularCodec(usecase)
            .orElseThrow();
    }
}
