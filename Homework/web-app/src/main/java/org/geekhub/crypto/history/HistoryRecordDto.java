package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;

import java.time.LocalDate;
import java.util.Objects;

public class HistoryRecordDto {
    private Integer id;
    private LocalDate date;
    private Operation operation;
    private String userInput;
    private Algorithm algorithm;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HistoryRecordDto that = (HistoryRecordDto) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            operation == that.operation &&
            Objects.equals(userInput, that.userInput) &&
            algorithm == that.algorithm;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, operation, userInput, algorithm);
    }

    @Override
    public String toString() {
        return "HistoryRecordDto{" +
            "id=" + id +
            ", date=" + date +
            ", operation=" + operation +
            ", userInput='" + userInput + '\'' +
            ", algorithm=" + algorithm +
            '}';
    }
}
