package org.geekhub.crypto.history;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/history")
public class CodingHistoryController {

//  TODO Implement DTO-s instead of using entities

    private final CodingHistory codingHistory;

    public CodingHistoryController(CodingHistory codingHistory) {
        this.codingHistory = codingHistory;
    }

    @GetMapping
    public List<HistoryRecord> getHistoryRecords() {
        return codingHistory.getHistoryRecords();
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addRecord(@RequestBody HistoryRecordDto historyRecord) {
        codingHistory.addRecord(fromHistoryRecordDto(historyRecord));
    }

    private HistoryRecord fromHistoryRecordDto(HistoryRecordDto historyRecordDto) {
        return HistoryRecord.of(
            historyRecordDto.getDate(),
            historyRecordDto.getOperation(),
            historyRecordDto.getUserInput(),
            historyRecordDto.getAlgorithm()
        );
    }

    @DeleteMapping("/remove-last-record")
    public void removeLastRecord() {
        codingHistory.removeLastRecord();
    }

    @DeleteMapping
    public void clearHistoryRecords() {
        codingHistory.clearHistoryRecords();
    }

}
