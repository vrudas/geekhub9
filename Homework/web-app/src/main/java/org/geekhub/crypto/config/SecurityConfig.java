package org.geekhub.crypto.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        var passwordEncoder = new BCryptPasswordEncoder();

        auth.inMemoryAuthentication()
            .passwordEncoder(passwordEncoder)
                .withUser("admin")
                    .password(passwordEncoder.encode("admin")).roles("ADMIN")
                    .and()
                .withUser("user")
                    .password(passwordEncoder.encode("user")).roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .mvcMatchers("/").permitAll()
            .antMatchers("/analytics/**").hasRole("ADMIN")
            .anyRequest().authenticated()
            .and().httpBasic()
            .and().csrf().disable();
    }
}
