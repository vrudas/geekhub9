package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CodingHistoryController.class)
public class CodingHistoryControllerTest extends AbstractTestNGSpringContextTests {

    private static final int YEAR = 2019;
    private static final int DAY_OF_MONTH = 14;

    @Autowired
    @MockBean
    private CodingHistory codingHistory;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void check_json_conversion_of_history_records_endpoint() throws Exception {
        var historyRecord = HistoryRecord.of(
            LocalDate.of(YEAR, Month.JANUARY, DAY_OF_MONTH),
            Operation.CODEC_DECODE,
            "text",
            Algorithm.CAESAR
        ).withId(1);

        var historyRecords = List.of(historyRecord);

        when(codingHistory.getHistoryRecords()).thenReturn(historyRecords);

        mockMvc.perform(
            get("/history")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").exists())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0]").exists())
            .andExpect(jsonPath("$.[0].id").value(1))
            .andExpect(jsonPath("$.[0].date").value("2019-01-14"))
            .andExpect(jsonPath("$.[0].operation").value("CODEC_DECODE"))
            .andExpect(jsonPath("$.[0].userInput").value("text"))
            .andExpect(jsonPath("$.[0].algorithm").value("CAESAR"));
    }

    @Test
    public void history_record_added_after_call_to_add_record_endpoint() throws Exception {
        var historyRecord = HistoryRecord.of(
            LocalDate.of(YEAR, Month.JANUARY, DAY_OF_MONTH),
            Operation.CODEC_DECODE,
            "text",
            Algorithm.CAESAR
        );

        String historyRecordJson = new JSONObject()
            .put("id", String.valueOf(1))
            .put("date", "2019-01-14")
            .put("operation", Operation.CODEC_DECODE.name())
            .put("userInput", "text")
            .put("algorithm", Algorithm.CAESAR.name())
            .toString();

        mockMvc.perform(
            put("/history")
                .contentType(MediaType.APPLICATION_JSON)
                .content(historyRecordJson)
                .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isCreated());

        verify(codingHistory).addRecord(historyRecord);
    }

    @Test
    public void last_history_record_removed_after_call_to_remove_last_record_endpoint() throws Exception {
        mockMvc.perform(
            delete("/history/remove-last-record")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isOk());

        verify(codingHistory).removeLastRecord();
    }

    @Test
    public void history_cleared_after_call_to_remove_all_history_records_endpoint() throws Exception {
        mockMvc.perform(
            delete("/history")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isOk());

        verify(codingHistory).clearHistoryRecords();
    }
}