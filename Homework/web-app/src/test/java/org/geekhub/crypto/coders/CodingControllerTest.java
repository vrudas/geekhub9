package org.geekhub.crypto.coders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CodingController.class)
public class CodingControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @SpyBean
    private CaesarCodec codec;

    @Autowired
    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        reset(codec);
    }

    @Test
    public void encode_url_not_available_for_anonymous_user() throws Exception {
        mockMvc.perform(
            post("/coding/encode/{codec}", Algorithm.CAESAR)
        ).andExpect(status().isUnauthorized());
    }

    @Test
    public void validate_encode_endpoint_response() throws Exception {
        String textToEncode = "text";
        String encodedText = textToEncode.toUpperCase();

        when(codec.encode(textToEncode)).thenReturn(encodedText);

        mockMvc.perform(
            post("/coding/encode/{codec}", Algorithm.CAESAR)
                .contentType(MediaType.APPLICATION_JSON)
                .param("input", textToEncode)
            .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isOk())
            .andExpect(content().string(encodedText));
    }

    @Test
    public void validate_decode_endpoint_response() throws Exception {
        String textToDecode = "text";
        String decodedText = textToDecode.toUpperCase();

        when(codec.decode(textToDecode)).thenReturn(decodedText);

        mockMvc.perform(
            post("/coding/decode/{codec}", Algorithm.CAESAR)
                .contentType(MediaType.APPLICATION_JSON)
                .param("input", textToDecode)
            .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isOk())
            .andExpect(content().string(decodedText));
    }

}