package org.geekhub.crypto.history;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;

@JdbcTest
public class HistoryRecordTableSchemaTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @BeforeMethod
    public void setUp() {
        JdbcTestUtils.deleteFromTables(
            namedJdbcTemplate.getJdbcTemplate(),
            "history_record"
        );
    }

    @Test(expectedExceptions = DataIntegrityViolationException.class)
    public void failed_to_insert_null_date_value() {
        var params = new MapSqlParameterSource();
        params.addValue("date", null);

        namedJdbcTemplate.update(
            "INSERT INTO history_record(date) VALUES (:date)",
            params
        );
    }

    @Test(expectedExceptions = DataIntegrityViolationException.class)
    public void failed_to_insert_null_operation_value() {
        var params = new MapSqlParameterSource();
        params.addValue("date", LocalDate.now());
        params.addValue("operation", null);

        namedJdbcTemplate.update(
            "INSERT INTO history_record(date, operation) VALUES (:date, :operation)",
            params
        );
    }

    @Test(expectedExceptions = DataIntegrityViolationException.class)
    public void failed_to_insert_null_user_input_value() {
        var params = new MapSqlParameterSource();
        params.addValue("date", LocalDate.now());
        params.addValue("operation", "CODEC_DECODE");
        params.addValue("user_input", null);

        namedJdbcTemplate.update(
            "INSERT INTO history_record(date, operation, user_input) " +
                "VALUES (:date, :operation, :user_input)",
            params
        );
    }

    @Test(expectedExceptions = DataIntegrityViolationException.class)
    public void failed_to_insert_null_algorithm_value() {
        var params = new MapSqlParameterSource();
        params.addValue("date", LocalDate.now());
        params.addValue("operation", "CODEC_DECODE");
        params.addValue("user_input", "");
        params.addValue("algorithm", null);

        namedJdbcTemplate.update(
            "INSERT INTO history_record(date, operation, user_input, algorithm) " +
                "VALUES (:date, :operation, :user_input, :algorithm)",
            params
        );
    }

}
