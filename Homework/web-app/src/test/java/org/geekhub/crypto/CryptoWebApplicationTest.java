package org.geekhub.crypto;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@SpringBootTest
public class CryptoWebApplicationTest extends AbstractTestNGSpringContextTests {

    @Test
    public void context_started() {
        CryptoWebApplication.main(new String[0]);
    }
}