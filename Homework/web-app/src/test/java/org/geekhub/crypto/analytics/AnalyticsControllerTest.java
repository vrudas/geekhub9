package org.geekhub.crypto.analytics;

import org.geekhub.crypto.coders.Algorithm;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyMap;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AnalyticsController.class)
public class AnalyticsControllerTest extends AbstractTestNGSpringContextTests {

    private static final int YEAR = 2019;
    private static final int DAY_OF_MONTH = 31;

    @Autowired
    @MockBean
    private CodingAudit codingAudit;

    @Autowired
    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        when(codingAudit.countEncodingInputs()).thenReturn(emptyMap());
    }

    @Test
    public void access_not_allowed_to_encoding_inputs_count_endpoint_for_plain_user() throws Exception {
        mockMvc.perform(
            get("/analytics/encoding-inputs-count")
                .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void access_allowed_to_encoding_inputs_count_endpoint_for_admin_user() throws Exception {
        mockMvc.perform(
            get("/analytics/encoding-inputs-count")
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    public void validate_encoding_inputs_count_endpoint_response_structure() throws Exception {
        when(codingAudit.countEncodingInputs()).thenReturn(Map.of("word", 1L));

        mockMvc.perform(
            get("/analytics/encoding-inputs-count")
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isMap())
            .andExpect(jsonPath("$", Matchers.hasEntry("word", 1)));
    }

    @Test
    public void validate_codings_by_date_endpoint_response_structure() throws Exception {
        when(codingAudit.countCodingsByDate(CodecUsecase.ENCODING))
            .thenReturn(
                Map.of(
                    LocalDate.of(YEAR, Month.JANUARY, DAY_OF_MONTH),
                    1L
                )
            );

        mockMvc.perform(
            get("/analytics/codings-by-date/{usecase}", CodecUsecase.ENCODING.name())
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isMap())
            .andExpect(jsonPath("$", Matchers.hasEntry("2019-01-31", 1)));
    }

    @Test
    public void validate_most_popular_codec_endpoint_response_structure() throws Exception {
        when(codingAudit.findMostPopularCodec(CodecUsecase.ENCODING))
            .thenReturn(Optional.of(Algorithm.MORSE));

        mockMvc.perform(
            get("/analytics/most-popular-codec/{usecase}", CodecUsecase.ENCODING.name())
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isString())
            .andExpect(jsonPath("$").value("MORSE"));
    }

    @Test
    public void validate_most_popular_codec_endpoint_error_response_structure() throws Exception {
        when(codingAudit.findMostPopularCodec(CodecUsecase.ENCODING))
            .thenReturn(Optional.empty());

        mockMvc.perform(
            get("/analytics/most-popular-codec/{usecase}", CodecUsecase.ENCODING.name())
                .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isInternalServerError());
    }

}