package org.geekhub.crypto.history;

import org.geekhub.crypto.coders.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;

@JdbcTest
public class HistoryRecordTableCRUDTest extends AbstractTestNGSpringContextTests {

    private static final RowMapper<HistoryRecord> HISTORY_RECORD_ROW_MAPPER = (rs, rowNum) -> HistoryRecord.of(
        rs.getDate("date").toLocalDate(),
        Operation.valueOf(rs.getString("operation")),
        rs.getString("user_input"),
        Algorithm.valueOf(rs.getString("algorithm"))
    ).withId(rs.getInt("id"));

    private static final int YEAR = 2019;
    private static final int DAY_OF_MONTH = 14;

    private static final HistoryRecord HISTORY_RECORD = HistoryRecord.of(
        LocalDate.of(YEAR, Month.JANUARY, DAY_OF_MONTH),
        Operation.CODEC_DECODE,
        "text",
        Algorithm.MORSE
    );

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Test
    public void crud_operations_was_success() {
        var insertedRecordId = performCreate();
        performRead(insertedRecordId);
        performUpdate(insertedRecordId);
        performDelete(insertedRecordId);
    }

    private int performCreate() {
        var historyRecordInsert = new SimpleJdbcInsert(namedJdbcTemplate.getJdbcTemplate())
            .withTableName("history_record")
            .usingGeneratedKeyColumns("id");

        var insertParams = new BeanPropertySqlParameterSource(HISTORY_RECORD);

        var insertedRecordId = historyRecordInsert.executeAndReturnKey(insertParams);
        assertThat(insertedRecordId).isEqualTo(1);

        return insertedRecordId.intValue();
    }

    private void performRead(int insertedRecordId) {
        var historyRecordFromDb = namedJdbcTemplate.queryForObject(
            "SELECT id, date, operation, user_input, algorithm FROM history_record WHERE id = :id",
            Map.of("id", insertedRecordId),
            HISTORY_RECORD_ROW_MAPPER
        );

        assertThat(historyRecordFromDb).isEqualTo(HISTORY_RECORD.withId(1));
    }

    private void performUpdate(int insertedRecordId) {
        var updateParams = new MapSqlParameterSource();
        updateParams.addValue("id", insertedRecordId);
        updateParams.addValue("user_input", "TEXT");

        namedJdbcTemplate.update(
            "UPDATE history_record SET user_input = :user_input",
            updateParams
        );

        var updatedHistoryRecordFromDb = namedJdbcTemplate.queryForObject(
            "SELECT id, date, operation, user_input, algorithm FROM history_record WHERE id = :id",
            Map.of("id", insertedRecordId),
            HISTORY_RECORD_ROW_MAPPER
        );

        assertThat(updatedHistoryRecordFromDb).isEqualTo(
            HistoryRecord.of(
                LocalDate.of(YEAR, Month.JANUARY, DAY_OF_MONTH), Operation.CODEC_DECODE,
                "TEXT",
                Algorithm.MORSE
            ).withId(1)
        );
    }

    private void performDelete(int insertedRecordId) {
        namedJdbcTemplate.update(
            "DELETE FROM history_record WHERE id = :id",
            Map.of("id", insertedRecordId)
        );

        var recordsCount = namedJdbcTemplate.queryForObject(
            "SELECT count(*) FROM history_record",
            emptyMap(),
            Integer.class
        );

        assertThat(recordsCount).isEqualTo(0);
    }
}
