package org.geekhub.crypto.history.db;

import org.geekhub.crypto.coders.Algorithm;
import org.geekhub.crypto.history.HistoryRecord;
import org.geekhub.crypto.history.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@DataJdbcTest
public class HistoryRecordDbRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private HistoryRecordDbRepository historyRecordDbRepository;

    @BeforeMethod
    public void setUp() {
        historyRecordDbRepository.deleteAll();
    }

    @Test
    public void no_history_records_in_db() {
        assertEquals(historyRecordDbRepository.count(), 0);
    }

    @Test
    public void getting_record_for_not_existing_id() {
        var historyRecordOptional = historyRecordDbRepository.findById(0);
        assertTrue(historyRecordOptional.isEmpty());
    }

    @Test
    public void nothing_happened_when_trying_to_delete_not_existing_record() {
        assertThatCode(() -> historyRecordDbRepository.deleteById(1))
            .doesNotThrowAnyException();
    }

    @Test
    public void insert_history_record() {
        assertEquals(historyRecordDbRepository.count(), 0);

        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.CAESAR
        );

        historyRecordDbRepository.save(historyRecord);

        assertEquals(historyRecordDbRepository.count(), 1);
    }

    @Test
    public void check_that_inserted_record_has_id() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "text",
            Algorithm.CAESAR
        );

        var actualHistoryRecord = historyRecordDbRepository.save(historyRecord);

        var historyRecordWithId = historyRecord.withId(1);
        assertEquals(actualHistoryRecord, historyRecordWithId);
    }

    @Test
    public void update_record_field_is_success() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "",
            Algorithm.CAESAR
        );

        var savedHistoryRecord = historyRecordDbRepository.save(historyRecord);

        var historyRecordWithUpdatedFields = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "text",
            Algorithm.CAESAR
        ).withId(savedHistoryRecord.getId());

        var actualHistoryRecord = historyRecordDbRepository.save(historyRecordWithUpdatedFields);

        assertEquals(actualHistoryRecord, historyRecordWithUpdatedFields);
    }

    @Test
    public void delete_record_was_success() {
        var historyRecord = HistoryRecord.of(
            Operation.CODEC_DECODE,
            "text",
            Algorithm.CAESAR
        );

        var actualHistoryRecord = historyRecordDbRepository.save(historyRecord);

        assertEquals(historyRecordDbRepository.count(), 1);

        historyRecordDbRepository.delete(actualHistoryRecord);

        assertEquals(historyRecordDbRepository.count(), 0);
    }
}
